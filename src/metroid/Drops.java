package metroid;

import java.util.Random;

public interface Drops {
	/**
	 * dona un 66% de posibilitat de que el enemic deixi anar un drop al morir, si es te els misils aquests drops surten el 50%
	 * de vegades que hi hagui drop, en cas contrari nomes es dropeja hp.
	 * @param e
	 */
	public static void enemy_drop(enemy e) {
		Random r = new Random();
		int num = r.nextInt(3);
		if (num != 0) {
			// int spawn_x = e.x1;
			int spawn_x = (((e.x2 - e.x1) / 2) - 5) + e.x1;
			// int spawn_y = e.y1;
			int spawn_y = (((e.y2 - e.y1) / 2) - 5) + e.y1;
			if (Main.samus.missile_unlocked && num == 2) {
				Missile_drop M_drop = new Missile_drop("", spawn_x, spawn_y, spawn_x + 15, spawn_y + 15,
						"sprites/drops/Missile_drop.gif");
				Mapa.drops.add(M_drop);
			} else {
				Hp_drop h_drop = new Hp_drop("", spawn_x, spawn_y, spawn_x + 15, spawn_y + 15,
						"sprites/drops/hp_drop.gif");
				Mapa.drops.add(h_drop);
			}
			
		}
		if (Samus.get_actual_map() == 3) {
			Main.f.add(Boss.camp);
		}
	}

}

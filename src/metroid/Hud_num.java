package metroid;

import motor.Sprite;

public class Hud_num extends Sprite {

	public Hud_num(int x1, int y1, int x2, int y2, String path) {
		super("", x1, y1, x2, y2, path);
		this.physicBody = false;
		this.terrain = false;
		this.solid = false;
		this.unscrollable = true;
		// TODO Auto-generated constructor stub
	}
	/**
	 * comprova el numero passat i canvia el numero per el corresponent
	 * @param num
	 */
	public void set_hud(int num) {
		switch (num) {
		case 1:
			this.path = "sprites/HUD/num_1.PNG";
			break;
			
		case 2:
			this.path = "sprites/HUD/num_2.PNG";
			break;
			
		case 3:
			this.path = "sprites/HUD/num_3.PNG";
			break;
			
		case 4:
			this.path = "sprites/HUD/num_4.PNG";
			break;
			
		case 5:
			this.path = "sprites/HUD/num_5.PNG";
			break;
			
		case 6:
			this.path = "sprites/HUD/num_6.PNG";
			break;
			
		case 7:
			this.path = "sprites/HUD/num_7.PNG";
			break;
			
		case 8:
			this.path = "sprites/HUD/num_8.PNG";
			break;
			
		case 9:
			this.path = "sprites/HUD/num_9.PNG";
			break;

		default:
			this.path = "sprites/HUD/num_0.PNG";
			break;
		}
		// TODO Auto-generated method stub
		
	}

}

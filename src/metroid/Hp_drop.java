package metroid;

public class Hp_drop extends Drop {

	public Hp_drop(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		// TODO Auto-generated constructor stub
	}
	/**
	 * dona 11 de hp a samus cuan �s recollit
	 */
	@Override
	public void action() {
		Main.samus.hp += 11;
		if (Main.samus.hp > Main.samus.max_hp) {
			Main.samus.hp = Main.samus.max_hp;
		}
	}

}

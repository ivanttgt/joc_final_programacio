package metroid;

import java.util.ArrayList;

public class Ball_hud extends HUD {

	private static boolean created = false;
	private Ball_hud(int x1, int y1, int x2, int y2, String path) {
		super(x1, y1, x2, y2, path);
	}
	/**
	 * retorna el hud de bola, si no existeix el crea
	 * @return
	 */
	public static Ball_hud get_ball_hud () {
		Ball_hud Ball_hud = null;
		if (!created) {
			Ball_hud = new Ball_hud(220, 10, 240, 30, "sprites/HUD/hud_ball.png");
		} else {
			Ball_hud.x1 = Main.samus.x1;
			Ball_hud.x2 = Main.samus.x2;
			Ball_hud.y1 = Main.samus.y1;
			Ball_hud.y2 = Main.samus.y2;
			Ball_hud.path = Main.samus.path;
		}
		return Ball_hud;
		
	}
	/**
	 * actualitxa el hud de bola per a fer-lo coincidir amb el estat de samus
	 */
	@Override
	public void set_hud(int hp) {
		if (Main.samus.samus_ball) {
			this.path = "sprites/HUD/choosed_ball.png";
		} else if (Main.samus.ball_unlocked) {
			this.path =  "sprites/HUD/unlocked_ball.png";
		}
	}

}

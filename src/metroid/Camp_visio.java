package metroid;

import motor.Sprite;

public class Camp_visio extends Sprite{
	
	private int h_step = 0;

	public Camp_visio(String name, int x1, int y1, int x2, int y2) {
		super(name, x1, y1, x2, y2,"");
		trigger = true;
				
	}
	/**
	 * expandeix el camp de visio horitzontalment fins que xoca amb terreny pels 2 costats, usat per geruboss (i el futur Boss).
	 * @param geruboss
	 */
	public void expansio_horitzontal(Geruboss geruboss) {
			// allarga costat deret
			if (h_step == 0) {
				this.x2 += 1;
				if (this.collidesWithList(Mapa.grounds).size()>0) {
					h_step++;
					this.x2 -= 8;
					
				}
			} else if (h_step == 1) { // allarga costat esquerra
				this.x1 -= 1;
				if (this.collidesWithList(Mapa.grounds).size()>0) {
					h_step++;
					this.x2 += 8;
					
				}
			} else {
					geruboss.set_camp_creat(true);
					System.out.println();
					Main.f.clear();
					Main.add_sprites(Main.f);
			}
		
	}

}

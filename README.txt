Controls:
w: saltar
a: more esquerra
d: moure dereta

f: canviar arma (mode no bola)
b: mode bola

espai: disparar/tirar bombes

Desbloquejar tot al principi:
Classe Samus: linees 17 i 18, booleans *_unlocked a TRUE
(sense tenir municio de misils des del principi, et pot aconsegur matant enemics)

Per tenir-ne:
Classe Samus: linea 40, num_missiles al numero de misils que  es vulguin (tindra errors de HUD amb mes de 99 i nomes es poden disparar si estan desbloquejats)

Passar-se el joc (arribar a obrir porta negra, de moment):
- caminar fins la porta superior dereta del 1r mapa, disparar-li
- baixar per la dereta del 2n mapa i trobar les bombes (pujant per les plataformes que hi ha sobre la lava, 
fa falta baitejar al enemic de la ultima plataforma (saltant) per poder arribar)
- tornar al primer mapa per on has vingut i destruir la paret biologica lila de abaix
- entrar en mode bola pel petit forat, entrant al 2n mapa i agafar els misils
- un cop agafats torna al mapa 1 per anar al lloc de la lava del mapa 2 i anar directe a la esquerra de aquesta on es trova la porta final (que portara a la sala del boss en un futur)

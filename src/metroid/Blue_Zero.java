package metroid;

public class Blue_Zero extends enemy {
	public Blue_Zero(String name, int x1, int y1, int x2, int y2) {
		super("BlueZ", x1, y1, x2, y2, "sprites/enemies/blue_zero/bz_l_move.gif");
		this.l_direction = false;
		this.hp = 30;
		solid = true;
		physicBody = true;
		this.setConstantForce(0, 1.2);
		this.setVelocity(-2, 0);
		// TODO Auto-generated constructor stub
	}
	/**
	 * es mou a la esquerra fins que colisiona, al colisionar canvia de direcci�
	 */
	public void passius() {
		if (this.collidesWith(Main.samus) || this.isOnColumn(Main.f)) {
			// si mira a la dereta
			if (this.l_direction) {
				this.setVelocity(-2, 0);
				this.l_direction = false;
				this.path = "sprites/enemies/blue_zero/bz_l_move.gif";

				// si mira a la esquerra
			} else {
				this.setVelocity(2, 0);
				this.l_direction = true;
				this.path = "sprites/enemies/blue_zero/bz_r_move.gif";
			}

		}

	}

}

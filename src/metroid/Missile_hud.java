package metroid;

import java.util.ArrayList;

public class Missile_hud extends HUD{

	private static boolean created = false;
	private static ArrayList<Hud_num> nums = new ArrayList<Hud_num>();
	private Missile_hud(int x1, int y1, int x2, int y2, String path) {
		super(x1, y1, x2, y2, path);
	}
	/**
	 * retorna el hud dels misils, en cas de no existir el crea
	 * @return
	 */
	public static Missile_hud get_missile_hud () {
		Missile_hud missile_hud = null;
		if (!created) {
			missile_hud = new Missile_hud(110, 10, 210, 40, "sprites/HUD/locked_missiles.PNG");
			nums.add(new Hud_num(missile_hud.x1+20, missile_hud.y1+18, missile_hud.x1+30, missile_hud.y1+28, "sprites/HUD/num_0.PNG"));
			nums.add(new Hud_num(missile_hud.x1+33, missile_hud.y1+18, missile_hud.x1+43, missile_hud.y1+28, "sprites/HUD/num_0.PNG"));
		} else {
			missile_hud.x1 = Main.samus.x1;
			missile_hud.x2 = Main.samus.x2;
			missile_hud.y1 = Main.samus.y1;
			missile_hud.y2 = Main.samus.y2;
			missile_hud.path = Main.samus.path;
		}
		return missile_hud;
		
	}
	/**
	 * retorna els numeros dinamics que composen el hud
	 * @return
	 */
	public static ArrayList<Hud_num> get_nums(){
		return nums;
		
	}
	/**
	 * actualitza el hud per a coincidir amb els misils actuals i si aquests estan seleccionats o no
	 */
	@Override
	public void set_hud(int hp) {
		int missiles_desena = Main.samus.num_missiles/10;
		int missiles_unitat = Main.samus.num_missiles - (missiles_desena*10);
		nums.get(0).set_hud(missiles_desena);
		nums.get(1).set_hud(missiles_unitat);
		if (Main.samus.actual_weapon == 2) {
			this.path = "sprites/HUD/choosed_missiles.PNG";
		} else if (Main.samus.missile_unlocked) {
			this.path =  "sprites/HUD/unlocked_missiles.PNG";
		}
	}

}

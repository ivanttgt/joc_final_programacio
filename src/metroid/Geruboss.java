package metroid;

import motor.Sprite;

public class Geruboss extends enemy{	
	
	private Camp_visio camp;
	private boolean camp_creat = false;
	private int jump_delay = 20;
	private boolean jump_capacity = true;

	public Geruboss(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		this.path = "sprites/enemies/geruboss/l_standing.png";
		this.l_direction = false;
		this.hp = 60;
		solid = true;
		physicBody = true;
		this.camp = crear_camp();
		this.setConstantForce(0, 0);
	}
	/**
	 * indica al enemic que el camp ha sigut creat per a poder comen�ar el moviment
	 * @param camp_creat
	 */
	public void set_camp_creat (boolean camp_creat) {
			this.camp_creat = camp_creat;
	}
	/**
	 * salta cap a la dereta o esquerra si samus colisiona amb ell o el seu camp de visio. canvia la direccio cada cop que xoca amb terreny
	 */
	@Override
	public void passius() {
		if (!camp_creat) {
			//System.out.println("SOC TO NULL");
			//	Main.f.add(this.camp);
		//	Main.add_sprites(Main.f);
			try {
				this.camp.expansio_horitzontal(this);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if ((this.collidesWith(Main.samus) || this.camp.collidesWith(Main.samus)) && jump_capacity) {
			jump_capacity = false;
		}
		if (!jump_capacity) {
			if (jump_delay >= 10) {
				jump_delay--;
				if (l_direction) {
					this.path = "sprites/enemies/geruboss/r_preparation_1.png";
				} else {
					this.path = "sprites/enemies/geruboss/l_preparation_1.png";
				}
			} else if (jump_delay >= 1) {
				jump_delay--;
				if (l_direction) {
					this.path = "sprites/enemies/geruboss/r_preparation_2.png";
				} else {
					this.path = "sprites/enemies/geruboss/l_preparation_2.png";
				}
			} else  if (jump_delay == 0) {
				if (l_direction) {
					this.jump_to_left();
				} else {
					this.jump_to_right();
				}
				jump_delay--;
			} else {
				if (this.collidesWithList(Mapa.grounds).size()>0) {
					this.setVelocity(0, 0);
					if (this.l_direction) {
						this.l_direction = false;
						this.path = "sprites/enemies/geruboss/l_standing.png";
					} else {
						this.l_direction = true;
						this.path = "sprites/enemies/geruboss/r_standing.png";
					}
					this.jump_delay = 20;
					this.jump_capacity = true;
				}
				else {
					if (l_direction) {
						this.jump_to_left();
					} else {
						this.jump_to_right();
					}
				}
				
			}
		}
				
	}
	/**
	 * salta cap a la dereta
	 */
	private void jump_to_right() {
		this.setVelocity(4, 0);
	}
	/**
	 * salta cap a la esquerra
	 */
	private void jump_to_left() {
		this.setVelocity(-4, 0);		
	}
	/**
	 * crea el camp de visio del enemic
	 * @return
	 */
	private  Camp_visio crear_camp() {
		
		Camp_visio camp = new Camp_visio("", this.x1+5, this.y1+5, this.x1+10, this.y1+15);
	//	camp.expansio_horitzontal();		
		return camp;
		
	}

}

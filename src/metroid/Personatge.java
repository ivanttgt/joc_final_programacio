package metroid;

import motor.Sprite;

public class Personatge extends Sprite{
	public static int hp;
	/**
	 * permet tenir hp a tot el que provingui de ell
	 * @param name
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param hp
	 * @param path
	 */
	public Personatge(String name, int x1, int y1, int x2, int y2, int hp, String path) {
		super(name, x1, y1, x2, y2, path);
		this.hp = hp;
		// TODO Auto-generated constructor stub
	}
}
package metroid;

import motor.Sprite;

public abstract class enemy extends Sprite implements Drops {
	int hp;
	boolean l_direction;
	public enemy(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		enemy.this.setConstantForce(0, 1.2);
	}
	/**
	 * obliga a les classes filles a tenir el metode
	 */
	public abstract void passius();
	/**
	 * mou tots els enemics del mapa actual
	 */
	public static void passius_enemies() {
		for (int i = 0; i < Mapa.enemies.size(); i++) {
			Mapa.enemies.get(i).passius();
		}		
	}
	
}

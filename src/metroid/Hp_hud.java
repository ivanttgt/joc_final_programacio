package metroid;

import java.util.ArrayList;

public class Hp_hud extends HUD {
	
	private static boolean created = false;
	private static ArrayList<Hud_num> nums = new ArrayList<Hud_num>();
	private Hp_hud(int x1, int y1, int x2, int y2, String path) {
		super(x1, y1, x2, y2, path);
	}
	/**
	 * retorna el hud de hp, si no existeix el crea
	 * @return
	 */
	public static Hp_hud get_hp_hud () {
		Hp_hud hp_hud = null;
		if (!created) {
			hp_hud = new Hp_hud(10, 10, 100, 40, "sprites/HUD/hp_hud.PNG");
			nums.add(new Hud_num(hp_hud.x1+17, hp_hud.y1+17, hp_hud.x1+27, hp_hud.y1+27, "sprites/HUD/num_0.PNG"));
			nums.add(new Hud_num(hp_hud.x1+30, hp_hud.y1+17, hp_hud.x1+40, hp_hud.y1+27, "sprites/HUD/num_0.PNG"));
			nums.add(new Hud_num(hp_hud.x1+80, hp_hud.y1+3, hp_hud.x1+85, hp_hud.y1+13, "sprites/HUD/num_0.PNG"));
		} else {
			hp_hud.x1 = Main.samus.x1;
			hp_hud.x2 = Main.samus.x2;
			hp_hud.y1 = Main.samus.y1;
			hp_hud.y2 = Main.samus.y2;
			hp_hud.path = Main.samus.path;
		}
		return hp_hud;
		
	}
	/**
	 * retorna els numeros dinamics que composen al hud
	 * @return
	 */
	public static ArrayList<Hud_num> get_nums(){
		return nums;
		
	}
	/**
	 * actualitza el hud per a correspondre amb el estat actual
	 */
	@Override
	public void set_hud(int hp) {
		int hp_desena = Main.samus.hp/10;
		int hp_unitat = Main.samus.hp - (hp_desena*10);
		int vides = Main.samus.lives;
		nums.get(0).set_hud(hp_desena);
		nums.get(1).set_hud(hp_unitat);
		nums.get(2).set_hud(vides);
	}

}

package metroid;

public class Missile_Unlocker extends Unlocker{
	
	private static boolean created = false;
	
	private Missile_Unlocker(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		trigger = true;
		// TODO Auto-generated constructor stub
	}
	/**
	 * crea el desbloquejador, en cas de ja existir nom�s el retorna
	 * @return
	 */
	public static Unlocker get_unlocker() {
		Missile_Unlocker m_unlocker = null;
		if (created) {
			m_unlocker.x1 = Mapa.misile_unlocker.x1;
			m_unlocker.x2 = Mapa.misile_unlocker.x2;
			m_unlocker.y1 = Mapa.misile_unlocker.y1;
			m_unlocker.y2 = Mapa.misile_unlocker.y2;
			m_unlocker.name = Mapa.misile_unlocker.name;
			m_unlocker.path = Mapa.misile_unlocker.path;
		} else {
			// nivell 1
			m_unlocker = new Missile_Unlocker("missile_unlocker", 240, 330, 280, 370, "sprites/drops/unlock_missiles.PNG");

			// nivell2
			 //samus = new Samus("Samus", 416, 339, 456, 389, 99, 10, "sprites/Samus/Rs_standing.PNG");
			 
			 //max_hp=99, x1=416, y1=339, x2=456, y2=389, hp=9, num_misils=10
			created = true;
		}
		return m_unlocker;
	}
	/**
	 * desbloqueja els misils
	 */
	@Override
	public void action() {
		Main.samus.missile_unlocked = true;
		Main.samus.num_missiles = Main.samus.max_num_missiles;
	}

}

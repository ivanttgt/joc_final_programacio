package metroid;

public class shielded_door extends door{
	/**
	 * porta que nomes s'obra amb misils
	 * @param name
	 * @param id
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param go_lvl
	 * @param go_door_id
	 * @param path
	 */
	public shielded_door(String name, int id, int x1, int y1, int x2, int y2, String go_lvl, int go_door_id,
			String path) {
		super(name, id, x1, y1, x2, y2, go_lvl, go_door_id, path, false);
		// TODO Auto-generated constructor stub
	}

}

package metroid;

import motor.Sprite;

public class Bomb extends Sprite {
	public static int dmg = 40;
	public int time_to_explote;
	private boolean exploted = false;

	public Bomb(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		this.time_to_explote = 20;
		trigger = true;
		// TODO Auto-generated constructor stub

	}
	/**
	 * resta cooldown a la bomba i la fa explotar al final.
	 * @param i
	 * @return
	 */
	public int explotar(int i) {
			if (this.time_to_explote > 0) {
				if (this.time_to_explote < 3) {
					this.x1 -= 10;
					this.x2 += 10;
					this.y1 -= 10;
					this.y2 += 10;
					this.path = "sprites/ball_bomb/explosion.PNG";
					if (!exploted) {
						Main.w.playSFX("sounds/bomb_explosion.wav");
						this.exploted = true;
					}
					
					this.colisions();
				}
				this.time_to_explote--;
				return i +1 ;
			} else {
				this.delete();
				Samus.bombs.remove(this);
				return i;
			}
	}
	/**
	 * comprova les colisions de la bomba cuan explota
	 */
	public void colisions() {
		if (this.collidesWithList(Mapa.enemies).size() > 0) {
			// saber amb quin enemic esta colisionant
			for (int j = 0; j < Mapa.enemies.size(); j++) {
				if (Mapa.enemies.get(j).collidesWith(this)) {
					Mapa.enemies.get(j).hp -= dmg;
					// mirar vida enemic i fer-lo morir si esta a 0
					if (Mapa.enemies.get(j).hp <= 0) {
						System.out.println("mort");
						Drops.enemy_drop(Mapa.enemies.get(j));
						Mapa.enemies.get(j).delete();
						Mapa.enemies.remove(Mapa.enemies.get(j));
						Main.add_sprites(Main.f);
					}

				}
			}
		}
		// si colisiona amb alguna porta i amb quina
		if (this.collidesWithList(Mapa.doors).size() > 0) {
			System.out.println("borram");
			// saber amb quin enemic esta colisionant
			for (int j = 0; j < Mapa.doors.size(); j++) {
				if (Mapa.doors.get(j).collidesWith(this) && !(Mapa.doors.get(j) instanceof shielded_door)) {
					Mapa.doors.get(j).open_door();

				} else if (Mapa.doors.get(j).collidesWith(this) && Mapa.doors.get(j) instanceof shielded_door){
					Main.w.playSFX("sounds/error_door.wav");
				}
			}
		}
		if (this.collidesWithList(Mapa.destroyable_grounds).size() > 0) {
			System.out.println("borram");
			// saber amb quin enemic esta colisionant
			for (int j = 0; j < Mapa.destroyable_grounds.size(); j++) {
				if (Mapa.destroyable_grounds.get(j).collidesWith(this)) {
					Mapa.destroyable_grounds.get(j).delete();

				}
			}
		}
	}
}

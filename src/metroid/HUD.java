package metroid;

import java.util.ArrayList;

import motor.Sprite;

public abstract class HUD extends Sprite {
	
	public static ArrayList<HUD> huds = new ArrayList<HUD>();
	public static ArrayList<Hud_num> sub_huds = new ArrayList<Hud_num>();
	
	public HUD(int x1, int y1, int x2, int y2, String path) {
		super("HUD", x1, y1, x2, y2, path);
		this.physicBody = false;
		this.terrain = false;
		this.solid = false;
		this.unscrollable = true;
	}
	/**
	 * obliga tenir el metode a les classes filles
	 * @param num
	 */
	public abstract void set_hud(int num);
	/**
	 * carrega els huds i els numeros que els composen
	 */
	public static void get_huds(){
		huds.add(Hp_hud.get_hp_hud());
		sub_huds.addAll(Hp_hud.get_nums());
		huds.add(Missile_hud.get_missile_hud());
		sub_huds.addAll(Missile_hud.get_nums());
		huds.add(Ball_hud.get_ball_hud());
	}
	/**
	 * fa actualitzar tots els huds
	 */
	public static void set_huds() {
		for (int i = 0; i < huds.size(); i++) {
			huds.get(i).set_hud(Main.samus.hp);
		}
			
			
	}

}

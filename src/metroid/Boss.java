package metroid;

import java.util.ArrayList;

public class Boss extends enemy{
	
	
	public static Camp_visio camp;
	private boolean camp_creat = false;
	private int actual_frame = 1;
	private int frame_cooldown = 1;
	private int shoot_cooldowns = 1;
	private int shoot_delay = 5;
	private boolean l_direction;
	public static boolean defeated = false;
	private ArrayList<String> animation_1 = new ArrayList<String>();
	private ArrayList<String> animation_2 = new ArrayList<String>();
	private ArrayList<String> animation_3 = new ArrayList<String>();

	public Boss(String name, int x1, int y1, int x2, int y2) {
		super(name, x1, y1, x2, y2, "");
		this.charge_sprites_1();
		this.charge_sprites_2();
		this.charge_sprites_3();
		this.setConstantForce(0, 0);
		this.setVelocity(4, 0);
		this.hp = 200;
		camp = this.crear_camp();
		Main.f.add(camp);
		
		
	}

	@Override
	public void passius() {
			System.out.println("estic creat");
			System.out.println(this.camp.y1 + " " + this.camp.y2);
		this.animation_shoot();
		this.camp.setVelocity(this.velocity[0], 0);
		this.move();
	}
	
	private void move() {
		if (this.collidesWithList(Mapa.grounds).size()>0 || this.collidesWithList(Mapa.doors).size()>0) {
			if (this.l_direction == false) {
				this.setVelocity(4, 0);
				//this.camp.setVelocity(this.velocity[0], 0);
				this.l_direction = true;
			} else {
				this.setVelocity(-4, 0);
				//this.camp.setVelocity(-4, 0);
				this.l_direction = false;
			}
		} else {
			this.setVelocity(this.velocity[0], 0);
		}
	}

	private void charge_sprites_1(){
		animation_1.add("sprites/enemies/boss/1/f1_boss1.png");
		animation_1.add("sprites/enemies/boss/1/f2_boss1.png");
		animation_1.add("sprites/enemies/boss/1/f3_boss1.png");
		animation_1.add("sprites/enemies/boss/1/f4_boss1.png");
		animation_1.add("sprites/enemies/boss/1/f5_boss1.png");
		animation_1.add("sprites/enemies/boss/1/f6_boss1.png");
		animation_1.add("sprites/enemies/boss/1/f7_boss1.png");
		animation_1.add("sprites/enemies/boss/1/f8_boss1.png");
		
	}
	
	private void charge_sprites_2(){
		animation_2.add("sprites/enemies/boss/2/f1_boss2.png");
		animation_2.add("sprites/enemies/boss/2/f2_boss2.png");
		animation_2.add("sprites/enemies/boss/2/f3_boss2.png");
		animation_2.add("sprites/enemies/boss/2/f4_boss2.png");
		animation_2.add("sprites/enemies/boss/2/f5_boss2.png");
		animation_2.add("sprites/enemies/boss/2/f6_boss2.png");
		animation_2.add("sprites/enemies/boss/2/f7_boss2.png");
		animation_2.add("sprites/enemies/boss/2/f8_boss2.png");
	}
	
	private void charge_sprites_3(){
		animation_3.add("sprites/enemies/boss/3/f1_boss3.png");
		animation_3.add("sprites/enemies/boss/3/f2_boss3.png");
		animation_3.add("sprites/enemies/boss/3/f3_boss3.png");
		animation_3.add("sprites/enemies/boss/3/f4_boss3.png");
		animation_3.add("sprites/enemies/boss/3/f5_boss3.png");
		animation_3.add("sprites/enemies/boss/3/f6_boss3.png");
		animation_3.add("sprites/enemies/boss/3/f7_boss3.png");
		animation_3.add("sprites/enemies/boss/3/f8_boss3.png");
	}
	
	
	private Camp_visio crear_camp() {
		
		Camp_visio camp = new Camp_visio("", this.x1+55, this.y1+5, this.x1+65, this.y1+300);
	//	camp.expansio_horitzontal();		
		camp.trigger = true;
		//camp.setVelocity(2, 0);
		camp.path = "";
		System.out.println("aqui estic");
		return camp;
		
	}
	
	public void set_camp_creat (boolean camp_creat2) {
		this.camp_creat = camp_creat2;
		System.out.println(this.camp_creat);
		System.out.println(camp_creat);
}
	
	private void animation_shoot() {
		if(shoot_cooldowns <= 0 && this.camp.collidesWith(Main.samus) || shoot_delay > 0) {
			this.path = animation_3.get(actual_frame);
			shoot_cooldowns = 100;
			if (shoot_delay == 0) {
				shoot_delay = 10;
				foc foc = new foc ("foc", this.x1+50, this.y1+90, this.x1+70, this.y1+110);
				Mapa.enemies.add(foc);
				Main.add_sprites(Main.f);
				Main.f.add(Boss.camp);
			}
			shoot_delay --;
		} else if(shoot_cooldowns <= 0) {
			this.path = animation_2.get(actual_frame);
		} else {
			this.path = animation_1.get(actual_frame);
		}
		
		if (this.frame_cooldown == 0) {
			actual_frame++;
			this.frame_cooldown = 3;
		}
		if (shoot_delay == 0) {
			shoot_cooldowns--;
		}
		this.frame_cooldown--;
		if (actual_frame > animation_1.size()-1) {
			actual_frame = 0;
		}
	}

}

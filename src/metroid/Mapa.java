package metroid;


import java.util.ArrayList;

import javax.swing.text.StyledEditorKit.ForegroundAction;

import motor.Field;
import motor.Sprite;
import motor.Window;

public class Mapa{
	static ArrayList<Sprite> grounds = new ArrayList<Sprite>();
	static ArrayList<Sprite> destroyable_grounds = new ArrayList<Sprite>();
	static ArrayList<Sprite> lavas = new ArrayList<Sprite>();
	static ArrayList<door> doors = new ArrayList<door>();
	static ArrayList<enemy> enemies = new ArrayList<enemy>();
	static ArrayList<Drop> drops = new ArrayList<Drop>();
	static ArrayList<Unlocker> Unlockers = new ArrayList<Unlocker>();
	public static Unlocker misile_unlocker = Missile_Unlocker.get_unlocker();
	public static Unlocker ball_unlocker = Ball_Unlocker.get_unlocker();
	public static int h_start_scroll = 0;
	public static int h_end_scroll = 0;
	public static int v_start_scroll = 0;
	public static int v_end_scroll = 0;
	/**
	 * crida a les funcions que posen els limits: superior, inferior, dret i esquerra del mapa
	 */
	private static void posar_limits() {
		int max_right;
		int max_bottom;
		left_limit();
		max_right = right_limit();
		top_limit();
		max_bottom = bottom_limit();
		set_scroll(max_right, max_bottom);
	}
	/**
	 * delimita cuan es fara scroll o cuan no
	 * @param max_right
	 * @param max_bottom
	 */
	private static void set_scroll(int max_right, int max_bottom) {
		h_start_scroll = 307;
		h_end_scroll = (max_right - 340);
		v_start_scroll = 200;
		v_end_scroll = (max_bottom - 200);
		
	}
	/**
	 * carrega el nivell1
	 */
	public static void nivel1() {
		grounds.clear();
		destroyable_grounds.clear();
		lavas.clear();
		doors.clear();
		enemies.clear();
		drops.clear();
		Unlockers.clear();
		Samus.shoots.clear();
		Samus.set_actual_map(1);
		Main.f.background = "background1.PNG";
		
		// limit superior
		Ground tgplt1 = new Ground("tg_pl", 20, 0, 120, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(tgplt1);
		Ground tgplt2 = new Ground("tg_pl", 120, 0, 220, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(tgplt2);
		Ground tgplt3 = new Ground("tg_pl", 220, 0, 320, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(tgplt3);
		Ground tgplt4 = new Ground("tg_pl", 320, 0, 420, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(tgplt4);
		Ground tgplt5 = new Ground("tg_pl", 420, 0, 520, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(tgplt5);
		Ground tgplt6 = new Ground("tg_pl", 520, 0, 620, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(tgplt6);
		Ground gtl7 = new Ground("tg_pl", 620, 0, 720, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(gtl7);
		Ground gtl8 = new Ground("tg_pl", 720, 0, 820, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(gtl8);
		Ground gtl9 = new Ground("tg_pl", 820, 0, 920, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(gtl9);
		Ground gtl10 = new Ground("tg_pl", 920, 0, 1020, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(gtl10);
		Ground gtl11 = new Ground("tg_pl", 1020, 0, 1120, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(gtl11);
		Ground gtl12 = new Ground("tg_pl", 1120, 0, 1220, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(gtl12);
		Ground gtl13 = new Ground("tg_pl", 1220, 0, 1320, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(gtl13);
		// cap a la dereta del mapa
		Ground gg1 = new Ground("sprites/map/grounds/tg_gir1", 1320, 0, 1340, 20, "sprites/map/grounds/tg_gir1.png");
		grounds.add(gg1);
		Ground grl1 = new Ground("tg_pl", 1320, 20, 1340, 120, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(grl1);
		door door1 = new door("", 1, 1320, 120, 1360, 190,"nivell2",1, "sprites/map/doors/closed_right_door.png", false);
		doors.add(door1);
		//grounds 3
		Ground gg11 = new Ground("tg_pl", 1320, 190, 1340, 210, "sprites/map/grounds/tg_gir2.png");
		grounds.add(gg11);
		Ground g9 = new Ground("tg_pl", 1220, 190, 1320, 210, "sprites/map/grounds/tg_pla.png");
		grounds.add(g9);
		Ground gg5 = new Ground("tg_pl", 1200, 190, 1220, 210, "sprites/map/grounds/tg_gir4.png");
		grounds.add(gg5);
		Ground g10 = new Ground("tg_pl", 1200, 210, 1220, 310, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g10);
		Ground gg6 = new Ground("tg_pl", 1200, 310, 1220, 330, "sprites/map/grounds/tg_gir2.png");
		grounds.add(gg6);
		Ground g11 = new Ground("tg_pl", 1100, 310, 1200, 330, "sprites/map/grounds/tg_pla.png");
		grounds.add(g11);
		Ground g12 = new Ground("tg_pl", 1000, 310, 1100, 330, "sprites/map/grounds/tg_pla.png");
		grounds.add(g12);
		Ground g13 = new Ground("tg_pl", 900, 310, 1000, 330, "sprites/map/grounds/tg_pla.png");
		grounds.add(g13);
		Ground g14 = new Ground("tg_pl", 800, 310, 900, 330, "sprites/map/grounds/tg_pla.png");
		grounds.add(g14);
		Ground g15 = new Ground("tg_pl", 700, 310, 800, 330, "sprites/map/grounds/tg_pla.png");
		grounds.add(g15);
		Ground gg7 = new Ground("tg_pl", 680, 310, 700, 330, "sprites/map/grounds/tg_gir4.png");
		grounds.add(gg7);
		Ground g16 = new Ground("tg_pl", 680, 330, 700, 430, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g16);
		Ground gg8 = new Ground("tg_pl", 680, 430, 700, 450, "sprites/map/grounds/tg_gir2.png");
		grounds.add(gg8);
		Ground g17 = new Ground("tg_pl", 580, 430, 680, 450, "sprites/map/grounds/tg_pla.png");
		grounds.add(g17);
		Ground g18 = new Ground("tg_pl", 480, 430, 580, 450, "sprites/map/grounds/tg_pla.png");
		grounds.add(g18);
		Ground g19 = new Ground("tg_pl", 380, 430, 480, 450, "sprites/map/grounds/tg_pla.png");
		grounds.add(g19);
		
		Ground g37 = new Ground("tg_pl", 240, 530, 280, 550, "sprites/map/grounds/air_ground.png");
		grounds.add(g37);

		
		// cap a la esquerra del mapa
		// limit esquerra
		Ground tgg4 = new Ground("sprites/map/grounds/tg_gir1", 0, 0, 20, 20, "sprites/map/grounds/tg_gir4.png");
		grounds.add(tgg4);
		Ground tgpll1 = new Ground("tg_pl", 0, 20, 20, 120, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(tgpll1);
		Ground gll2 = new Ground("", 0, 120, 20, 220, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(gll2);
		Ground gg2 = new Ground("sprites/map/grounds/tg_gir1", 0, 220, 20, 240, "sprites/map/grounds/tg_gir3.png");
		grounds.add(gg2);
		
		// grounds 1
		Ground g1 = new Ground("sprites/map/grounds/tg_gir1", 20, 220, 120, 240, "sprites/map/grounds/tg_pla.png");
		grounds.add(g1);
		Ground g2 = new Ground("sprites/map/grounds/tg_gir1", 120, 220, 220, 240, "sprites/map/grounds/tg_pla.png");
		grounds.add(g2);
		Ground gg3 = new Ground("sprites/map/grounds/tg_gir1", 220, 220, 240, 240, "sprites/map/grounds/tg_gir1.png");
		grounds.add(gg3);
		
		// baixada
		Ground gb1 = new Ground("tg_pl", 220, 240, 240, 340, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(gb1);
		Ground gb2 = new Ground("tg_pl", 220, 340, 240, 440, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(gb2);
		Ground gb3 = new Ground("tg_pl", 220, 440, 240, 540, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(gb3);
		Ground gb4 = new Ground("tg_pl", 220, 540, 240, 640, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(gb4);
		
		// grounds 2
		Ground gg4 = new Ground("sprites/map/grounds/tg_gir1", 220, 640, 240, 660, "sprites/map/grounds/tg_gir3.png");
		grounds.add(gg4);
		Ground g3 = new Ground("sprites/map/grounds/tg_gir1", 240, 640, 340, 660, "sprites/map/grounds/tg_pla.png");
		grounds.add(g3);
		Ground g4 = new Ground("sprites/map/grounds/tg_gir1", 340, 640, 440, 660, "sprites/map/grounds/tg_pla.png");
		grounds.add(g4);
		Ground g5 = new Ground("sprites/map/grounds/tg_gir1", 440, 640, 540, 660, "sprites/map/grounds/tg_pla.png");
		grounds.add(g5);
		Ground g6 = new Ground("sprites/map/grounds/tg_gir1", 540, 640, 640, 660, "sprites/map/grounds/tg_pla.png");
		grounds.add(g6);
		Ground g7 = new Ground("sprites/map/grounds/tg_gir1", 640, 640, 740, 660, "sprites/map/grounds/tg_pla.png");
		grounds.add(g7);
		Ground g8 = new Ground("sprites/map/grounds/tg_gir1", 740, 640, 840, 660, "sprites/map/grounds/tg_pla.png");
		grounds.add(g8);
		Ground gg9 = new Ground("sprites/map/grounds/tg_gir1", 840, 640, 860, 660, "sprites/map/grounds/tg_gir2.png");
		grounds.add(gg9);
		Ground g20 = new Ground("sprites/map/grounds/tg_gir1", 840, 470, 860, 570, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g20);
		Ground g21 = new Ground("sprites/map/grounds/tg_gir1", 845, 570, 875, 640, "sprites/map/grounds/destroyable.gif");
		destroyable_grounds.add(g21);
		
		Ground gg10 = new Ground("sprites/map/grounds/tg_gir1", 840, 450, 860, 470, "sprites/map/grounds/tg_gir1.png");
		grounds.add(gg10);
		Ground gt3 = new Ground("sprites/map/grounds/tg_gir1", 750, 450, 840, 470, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(gt3);
		Ground gt4 = new Ground("sprites/map/grounds/tg_gir1", 660, 450, 750, 470, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(gt4);
		Ground gt5 = new Ground("sprites/map/grounds/tg_gir1", 570, 450, 660, 470, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(gt5);
		Ground gt6 = new Ground("sprites/map/grounds/tg_gir1", 480, 450, 570, 470, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(gt6);
		Ground gt7 = new Ground("sprites/map/grounds/tg_gir1", 380, 450, 480, 470, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(gt7);
		Ground gg12 = new Ground("sprites/map/grounds/tg_gir1", 360, 450, 380, 470, "sprites/map/grounds/tg_gir3.png");
		grounds.add(gg12);
		Ground gg13 = new Ground("sprites/map/grounds/tg_gir1", 360, 430, 380, 450, "sprites/map/grounds/tg_gir4.png");
		grounds.add(gg13);
		
		// sala secreta
		Ground g22 = new Ground("sprites/map/grounds/tg_gir1", 860, 470, 880, 570, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(g22);
		Ground g24 = new Ground("sprites/map/grounds/tg_gir1", 860, 640, 880, 660, "sprites/map/grounds/tg_gir3.png");
		grounds.add(g24);
		Ground g25 = new Ground("sprites/map/grounds/tg_gir1", 860, 450, 880, 470, "sprites/map/grounds/tg_gir4.png");
		grounds.add(g25);
		//en sentit del rellotge
		Ground g26 = new Ground("sprites/map/grounds/tg_gir1", 880, 450, 980, 470, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g26);
		Ground g27 = new Ground("sprites/map/grounds/tg_gir1", 980, 450, 1080, 470, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g27);
		Ground g28 = new Ground("sprites/map/grounds/tg_gir1", 1080, 450, 1180, 470, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g28);
		Ground g29 = new Ground("sprites/map/grounds/tg_gir1", 1180, 450, 1200, 470, "sprites/map/grounds/tg_gir1.png");
		grounds.add(g29);
		Ground g30 = new Ground("sprites/map/grounds/tg_gir1", 1180, 470, 1200, 595, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g30);
		Ground g35 = new Ground("sprites/map/grounds/tg_gir1", 1180, 595, 1200, 615, "sprites/map/grounds/tg_gir3.png");
		grounds.add(g35);
		Ground g36 = new Ground("sprites/map/grounds/tg_gir1", 1200, 595, 1340, 615, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g36);
		
		// en sentit contrarrellotge des de terra destruible
		Ground g31 = new Ground("sprites/map/grounds/tg_gir1", 880, 640, 995, 660, "sprites/map/grounds/tg_pla.png");
		grounds.add(g31);
		Ground g32 = new Ground("sprites/map/grounds/tg_gir1", 995, 640, 1110, 660, "sprites/map/grounds/tg_pla.png");
		grounds.add(g32);
		Ground g33 = new Ground("sprites/map/grounds/tg_gir1", 1110, 640, 1215, 660, "sprites/map/grounds/tg_pla.png");
		grounds.add(g33);
		Ground g34 = new Ground("sprites/map/grounds/tg_gir1", 1215, 640, 1340, 660, "sprites/map/grounds/tg_pla.png");
		grounds.add(g34);
		// porta invisible oberta
		door door2 = new door("", 2, 1320, 570, 1360, 640,"nivell2",2, "sprites/map/doors/opened_right_door", true);
		doors.add(door2);
		
		// posar enemics
		Blue_Zero bz1 = new Blue_Zero("", 620, 520, 670, 550);
		enemies.add(bz1);
		
		posar_limits();
	}
	/**
	 * carrega el nivell 2
	 */
	public static void nivel2() {
		grounds.clear();
		destroyable_grounds.clear();
		lavas.clear();
		doors.clear();
		drops.clear();
		enemies.clear();
		Unlockers.clear();
		Samus.shoots.clear();
		Samus.set_actual_map(2);
		Main.f.background = "background2.PNG";
		Ground g1 = new Ground("", 0, 0, 20, 20, "sprites/map/grounds/tg_gir4.png");
		grounds.add(g1);
		Ground g2 = new Ground("", 20, 0, 120, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g2);
		Ground g3 = new Ground("", 120, 0, 220, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g3);
		Ground g4 = new Ground("", 220, 0, 320, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g4);
		Ground g5 = new Ground("", 320, 0, 420, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g5);
		Ground g6 = new Ground("", 420, 0, 520, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g6);
		Ground g7 = new Ground("", 520, 0, 620, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g7);
		Ground g8 = new Ground("", 620, 0, 720, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g8);
		Ground g9 = new Ground("", 720, 0, 820, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g9);
		Ground g10 = new Ground("", 820, 0, 920, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g10);
		Ground g11 = new Ground("", 920, 0, 1020, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g11);
		Ground g12 = new Ground("", 1020, 0, 1120, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g12);
		Ground g14 = new Ground("", 0, 20, 20, 120, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(g14);
		Ground g15 = new Ground("", 0, 120, 20, 140, "sprites/map/grounds/tg_gir2.png");
		grounds.add(g15);
		door door1 = new door("", 1, -20, 140, 20, 210, "nivell1", 1, "sprites/map/doors/closed_left_door.png", false);
		doors.add(door1);
		Ground g16 = new Ground("", 0, 210, 20, 230, "sprites/map/grounds/tg_gir3.png");
		grounds.add(g16);
		
		// sala secreta
		Ground g17 = new Ground("", 0, 230, 20, 250, "sprites/map/grounds/tg_gir4.png");
		grounds.add(g17);
		Ground g18 = new Ground("", 20, 230, 120, 250, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g18);
		Ground g19 = new Ground("", 120, 230, 220, 250, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g19);
		Ground g27 = new Ground("", 220, 230, 320, 250, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g27);
		Ground g20 = new Ground("", 320, 230, 340, 250, "sprites/map/grounds/tg_gir1.png");
		grounds.add(g20);
		Ground g21 = new Ground("", 320, 250, 340, 370, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g21);
		Ground g22 = new Ground("", 320, 370, 340, 390, "sprites/map/grounds/tg_gir2.png");
		grounds.add(g22);
		Ground g23 = new Ground("", 0, 250, 20, 350, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(g23);
		Ground g24 = new Ground("", 0, 350, 20, 450, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(g24);
		Ground g25 = new Ground("", 0, 450, 20, 470, "sprites/map/grounds/tg_gir3.png");
		grounds.add(g25);
		Ground g26 = new Ground("", 20, 450, 120, 470, "sprites/map/grounds/tg_pla.png");
		grounds.add(g26);
		
		Ground g28 = new Ground("", 120, 450, 220, 470, "sprites/map/grounds/tg_pla.png");
		grounds.add(g28);
		Ground g29 = new Ground("", 220, 450, 320, 470, "sprites/map/grounds/tg_pla.png");
		grounds.add(g29);
		Ground g30 = new Ground("", 220, 370, 320, 390, "sprites/map/grounds/tg_pla.png");
		grounds.add(g30);
		Ground g31 = new Ground("", 220, 390, 320, 410, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g31);
		Ground g32 = new Ground("", 200, 370, 220, 390, "sprites/map/grounds/tg_gir4.png");
		grounds.add(g32);
		Ground g33 = new Ground("", 200, 390, 220, 410, "sprites/map/grounds/tg_gir3.png");
		grounds.add(g33);
		
		Ground g34 = new Ground("", 320, 390, 420, 410, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g34);
		Ground g35 = new Ground("", 420, 390, 500, 410, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g35);
		Ground g36 = new Ground("", 500, 390, 520, 410, "sprites/map/grounds/tg_gir1.png");
		grounds.add(g36);
		Ground g37 = new Ground("", 500, 410, 520, 520, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g37);
		Ground g38 = new Ground("", 320, 450, 450, 470, "sprites/map/grounds/tg_pla.png");
		grounds.add(g38);
		Ground g39 = new Ground("", 450, 450, 470, 470, "sprites/map/grounds/tg_gir1.png");
		grounds.add(g39);
		Ground g40 = new Ground("", 450, 470, 470, 490, "sprites/map/grounds/tg_gir2.png");
		grounds.add(g40);
		Ground g41 = new Ground("", 500, 520, 520, 540, "sprites/map/grounds/tg_gir2.png");
		grounds.add(g41);
		Ground g42 = new Ground("", 350, 470, 450, 490, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g42);
		Ground g43 = new Ground("", 250, 470, 350, 490, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g43);
		Ground g44 = new Ground("", 150, 470, 250, 490, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g44);
		Ground g45 = new Ground("", 130, 470, 150, 490, "sprites/map/grounds/tg_gir4.png");
		grounds.add(g45);
		
		Ground g46 = new Ground("", 400, 520, 500, 540, "sprites/map/grounds/tg_pla.png");
		grounds.add(g46);
		Ground g47 = new Ground("", 300, 520, 400, 540, "sprites/map/grounds/tg_pla.png");
		grounds.add(g47);
		Ground g48 = new Ground("", 200, 520, 300, 540, "sprites/map/grounds/tg_pla.png");
		grounds.add(g48);
		Ground g49 = new Ground("", 180, 520, 200, 540, "sprites/map/grounds/tg_gir4.png");
		grounds.add(g49);
		Ground g50 = new Ground("", 180, 540, 200, 620, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g50);
		Ground g51 = new Ground("", 180, 620, 200, 640, "sprites/map/grounds/tg_gir2.png");
		grounds.add(g51);
		Ground g52 = new Ground("", 90, 620, 180, 640, "sprites/map/grounds/tg_pla.png");
		grounds.add(g52);
		Ground g53 = new Ground("", 0, 620, 90, 640, "sprites/map/grounds/tg_pla.png");
		grounds.add(g53);
		
		Ground g54 = new Ground("", 130, 490, 150, 570, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(g54);
		Ground g55 = new Ground("", 130, 570, 150, 590, "sprites/map/grounds/tg_gir2.png");
		grounds.add(g55);
		
		Ground g56 = new Ground("", 0, 570, 130, 590, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g56);
		
		door door2 = new door("", 2, -20, 550, 20, 640, "nivell1", 2, "opened_left_door", true);
		doors.add(door2);		
		
		
		// continuacio superior
		Ground g57 = new Ground("", 20, 210, 130, 230, "sprites/map/grounds/tg_pla.png");
		grounds.add(g57);
		Ground g58 = new Ground("", 130, 210, 240, 230, "sprites/map/grounds/tg_pla.png");
		grounds.add(g58);
		Ground g59 = new Ground("", 240, 210, 340, 230, "sprites/map/grounds/tg_pla.png");
		grounds.add(g59);
		
		Ground g60 = new Ground("", 340, 210, 360, 230, "sprites/map/grounds/tg_gir1.png");
		grounds.add(g60);
		
		Ground g61 = new Ground("", 340, 230, 360, 250, "sprites/map/grounds/tg_gir3.png");
		grounds.add(g61);
		Ground g62 = new Ground("", 360, 230, 460, 250, "sprites/map/grounds/tg_pla.png");
		grounds.add(g62);
		Ground g63 = new Ground("", 460, 230, 560, 250, "sprites/map/grounds/tg_pla.png");
		grounds.add(g63);
		Ground g64 = new Ground("", 560, 230, 660, 250, "sprites/map/grounds/tg_pla.png");
		grounds.add(g64);
		Ground g65 = new Ground("", 660, 230, 760, 250, "sprites/map/grounds/tg_pla.png");
		grounds.add(g65);
		Ground g66 = new Ground("", 760, 230, 860, 250, "sprites/map/grounds/tg_pla.png");
		grounds.add(g66);
		
		Blue_Zero bz1 = new Blue_Zero("", 620, 199, 670, 229);
		enemies.add(bz1);
		
		Ground g67 = new Ground("", 860, 230, 880, 250, "sprites/map/grounds/tg_gir2.png");
		grounds.add(g67);
		Ground g68 = new Ground("", 860, 210, 880, 230, "sprites/map/grounds/tg_gir4.png");
		grounds.add(g68);
		Ground g69 = new Ground("", 880, 210, 900, 230, "sprites/map/grounds/tg_gir1.png");
		grounds.add(g69);
		Ground g70 = new Ground("", 880, 230, 900, 250, "sprites/map/grounds/tg_gir3.png");
		grounds.add(g70);
		Ground g71 = new Ground("", 900, 230, 920, 250, "sprites/map/grounds/tg_gir1.png");
		grounds.add(g71);
		Ground g72 = new Ground("", 900, 250, 920, 350, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(g72);
		Ground g73 = new Ground("", 900, 350, 920, 450, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(g73);
		Ground g74 = new Ground("", 900, 450, 920, 550, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(g74);
		Ground g75 = new Ground("", 900, 550, 920, 650, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(g75);
		Ground g76 = new Ground("", 900, 650, 920, 750, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(g76);
		Ground g99 = new Ground("", 900, 750, 920, 770, "sprites/map/grounds/tg_gir2.png");
		grounds.add(g99);
		Ground g100 = new Ground("", 800, 750, 900, 770, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g100);
		Ground g101 = new Ground("", 700, 750, 800, 770, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g101);
		Ground g102 = new Ground("", 680, 750, 700, 770, "sprites/map/grounds/tg_gir3.png");
		grounds.add(g102);
		Ground g103 = new Ground("", 680, 650, 700, 750, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g103);
		Ground g104 = new Ground("", 680, 550, 700, 650, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g104);
		Ground g105 = new Ground("", 680, 450, 700, 550, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g105);
		Ground g106 = new Ground("", 680, 350, 700, 450, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g106);
		Ground g107 = new Ground("", 680, 250, 700, 350, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g107);
		
		// limit dereta
		Ground g77 = new Ground("", 1120, 0, 1140, 20, "sprites/map/grounds/tg_gir1.png");
		grounds.add(g77);
		Ground g78 = new Ground("", 1120, 20, 1140, 120, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g78);
		Ground g79 = new Ground("", 1120, 120, 1140, 220, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g79);
		Ground g80 = new Ground("", 1120, 220, 1140, 320, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g80);
		Ground g81 = new Ground("", 1120, 320, 1140, 420, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g81);
		Ground g82 = new Ground("", 1120, 420, 1140, 520, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g82);
		Ground g83 = new Ground("", 1120, 520, 1140, 620, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g83);
		Ground g84 = new Ground("", 1120, 620, 1140, 720, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g84);
		Ground g85 = new Ground("", 1120, 720, 1140, 820, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g85);
		Ground g86 = new Ground("", 1120, 820, 1140, 920, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g86);
		Ground g88 = new Ground("", 1120, 920, 1140, 920, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g88);
		Ground g89 = new Ground("", 1120, 920, 1140, 940, "sprites/map/grounds/tg_gir2.png");
		grounds.add(g89);
		Ground g90 = new Ground("", 1020, 920, 1120, 940, "sprites/map/grounds/tg_pla.png");
		grounds.add(g90);
		Ground g98 = new Ground("", 1020, 920, 1120, 940, "sprites/map/grounds/tg_pla.png");
		grounds.add(g98);
		Ground g91 = new Ground("", 920, 920, 1020, 940, "sprites/map/grounds/tg_pla.png");
		grounds.add(g91);
		Ground g92 = new Ground("", 820, 920, 920, 940, "sprites/map/grounds/tg_pla.png");
		grounds.add(g92);
		Ground g93 = new Ground("", 720, 920, 820, 940, "sprites/map/grounds/tg_pla.png");
		grounds.add(g93);
		Ground g94 = new Ground("", 620, 920, 720, 940, "sprites/map/grounds/tg_pla.png");
		grounds.add(g94);
		//aixo ha de se lava
		Ground g95 = new Ground("", 600, 920, 620, 940, "sprites/map/grounds/tg_gir3.png");
		grounds.add(g95);
		Ground g96 = new Ground("", 600, 900, 620, 920, "sprites/map/grounds/tg_gir1.png");
		grounds.add(g96);
		Ground g97 = new Ground("", 580, 900, 600, 920, "sprites/map/grounds/tg_gir4.png");
		grounds.add(g97);
		Ground g108 = new Ground("", 580, 920, 600, 940, "sprites/map/grounds/tg_gir2.png");
		grounds.add(g108);
		lava l1 = new lava(480, 900, 580, 920);
		lavas.add(l1);
		Ground g109 = new Ground("", 480, 920, 580, 940, "sprites/map/grounds/tg_pla.png");
		grounds.add(g109);
		Ground g118 = new Ground("", 380, 920, 480, 940, "sprites/map/grounds/tg_pla.png");
		grounds.add(g118);
		lava l2 = new lava(380, 900, 480, 920);
		lavas.add(l2);
		Ground g119 = new Ground("", 360, 920, 380, 940, "sprites/map/grounds/tg_gir3.png");
		grounds.add(g119);
		Ground g120 = new Ground("", 360, 900, 380, 920, "sprites/map/grounds/tg_gir1.png");
		grounds.add(g120);
		Ground g121 = new Ground("", 340, 900, 360, 920, "sprites/map/grounds/tg_gir4.png");
		grounds.add(g121);
		Ground g122 = new Ground("", 340, 920, 360, 940, "sprites/map/grounds/tg_gir2.png");
		grounds.add(g122);
		Ground g123 = new Ground("", 240, 920, 340, 940, "sprites/map/grounds/tg_pla.png");
		grounds.add(g123);
		Ground g124 = new Ground("", 130, 920, 240, 940, "sprites/map/grounds/tg_pla.png");
		grounds.add(g124);
		Ground g125 = new Ground("", 20, 920, 130, 940, "sprites/map/grounds/tg_pla.png");
		grounds.add(g125);
		Ground g126 = new Ground("", 0, 920, 20, 940, "sprites/map/grounds/tg_gir3.png");
		grounds.add(g126);
		shielded_door door3 = new shielded_door("", 3, -20, 850, 20, 920, "nivell3", 1, "sprites/map/doors/closed_left_shielded_door.png");
		doors.add(door3);	
		Ground g127 = new Ground("", 0, 755, 20, 850, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(g127);
		Ground g128 = new Ground("", 0, 660, 20, 755, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(g128);
		Ground g129 = new Ground("", 0, 640, 20, 660, "sprites/map/grounds/tg_gir1.png");
		grounds.add(g129);

		//FINS AQUI LA LAVA
		
		// continua el terra
		
		//plataformes aerees
		//retorn primer mapaa
		Ground g110 = new Ground("", 1070, 780, 1120, 800, "sprites/map/grounds/air_ground.png");
		grounds.add(g110);
		Ground g111 = new Ground("", 920, 630, 970, 650, "sprites/map/grounds/air_ground.png");
		grounds.add(g111);
		Ground g112 = new Ground("", 1070, 480, 1120, 500, "sprites/map/grounds/air_ground.png");
		grounds.add(g112);
		Ground g113 = new Ground("", 920, 330, 970, 350, "sprites/map/grounds/air_ground.png");
		grounds.add(g113);
		
		
		
		// acces a bombes
		Ground g114 = new Ground("", 630, 630, 680, 650, "sprites/map/grounds/air_ground.png");
		grounds.add(g114);
		Ground g115 = new Ground("", 520, 480, 570, 500, "sprites/map/grounds/air_ground.png");
		grounds.add(g115);
		Ground g116 = new Ground("", 420, 780, 470, 800, "sprites/map/grounds/air_ground.png");
		grounds.add(g116);
		Ground g117 = new Ground("", 470, 780, 520, 800, "sprites/map/grounds/air_ground.png");
		grounds.add(g117);
		
		// enemics
				Geruboss grb1 = new Geruboss("", 923, 360, 963, 400, "");
				enemies.add(grb1);
				
				Geruboss grb2 = new Geruboss("", 523, 435, 563, 475, "");
				enemies.add(grb2);
				
				Geruboss grb3 = new Geruboss("", 923, 653, 963, 693, "");
				enemies.add(grb3);
				
				Unlockers.add(misile_unlocker);
				Unlockers.add(ball_unlocker);
		posar_limits();
	}
	
	
	public static void nivel3() {
		grounds.clear();
		destroyable_grounds.clear();
		lavas.clear();
		doors.clear();
		drops.clear();
		enemies.clear();
		Unlockers.clear();
		Samus.shoots.clear();
		Samus.set_actual_map(3);
		Main.f.setScrollx(0);
		Main.f.setScrolly(0);
		Main.f.background = "background2.PNG";
		Ground g1 = new Ground("", 0, 0, 20, 20, "sprites/map/grounds/tg_gir4.png");
		grounds.add(g1);
		Ground g2 = new Ground("", 20, 0, 120, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g2);
		Ground g3 = new Ground("", 120, 0, 220, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g3);
		Ground g4 = new Ground("", 220, 0, 320, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g4);
		Ground g5 = new Ground("", 320, 0, 420, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g5);
		Ground g6 = new Ground("", 420, 0, 530, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g6);
		Ground g7 = new Ground("", 530, 0, 644, 20, "sprites/map/grounds/tg_t_limit.png");
		grounds.add(g7);
		
		Ground g8 = new Ground("", 0, 340, 20, 360, "sprites/map/grounds/tg_gir3.png");
		grounds.add(g8);
		Ground g9 = new Ground("", 0, 20, 20, 120, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(g9);
		Ground g10 = new Ground("", 0, 120, 20, 230, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(g10);
		Ground g11 = new Ground("", 0, 230, 20, 340, "sprites/map/grounds/tg_l_limit.png");
		grounds.add(g11);
		
		Ground g12 = new Ground("", 644, 340, 664, 360, "sprites/map/grounds/tg_gir2.png");
		grounds.add(g12);
		Ground g13 = new Ground("", 20, 340, 120, 360, "sprites/map/grounds/tg_pla.png");
		grounds.add(g13);
		Ground g14 = new Ground("", 120, 340, 220, 360, "sprites/map/grounds/tg_pla.png");
		grounds.add(g14);
		Ground g15 = new Ground("", 220, 340, 320, 360, "sprites/map/grounds/tg_pla.png");
		grounds.add(g15);
		Ground g16 = new Ground("", 320, 340, 420, 360, "sprites/map/grounds/tg_pla.png");
		grounds.add(g16);
		Ground g17 = new Ground("", 420, 340, 530, 360, "sprites/map/grounds/tg_pla.png");
		grounds.add(g17);
		Ground g18 = new Ground("", 530, 340, 644, 360, "sprites/map/grounds/tg_pla.png");
		grounds.add(g18);
		
		Ground g19 = new Ground("", 644, 0, 664, 20, "sprites/map/grounds/tg_gir1.png");
		grounds.add(g19);
		door d1 = new door("", 1, 644, 20, 684, 90, "nivell2", 3, "sprites/map/doors/closed_right_door.png", false);
		doors.add(d1);
		Ground g24 = new Ground("", 644, 90, 664, 110, "sprites/map/grounds/tg_gir4.png");
		grounds.add(g24);
		Ground g21 = new Ground("", 644, 110, 664, 220, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g21);
		Ground g22 = new Ground("", 644, 220, 664, 340, "sprites/map/grounds/tg_r_limit.png");
		grounds.add(g22);;
		
		Boss aranya = new Boss("aranya", 30, 30, 150, 130);
		enemies.add(aranya);
		
		posar_limits();
	}
	
	
	/**
	 * posa el limit inferior del mapa
	 */
	private static int bottom_limit() {
		int blx2 = 0;
		int bly1 = 0;
		for (int i = 0; i < grounds.size(); i++) {
			if (grounds.get(i).y2 > bly1) {
				bly1 = grounds.get(i).y2;
			}
			if (grounds.get(i).x2 > blx2) {
				blx2 = grounds.get(i).x2;
			}
		}
		for (int i = 0; i < lavas.size(); i++) {
			if (lavas.get(i).y2 > bly1) {
				bly1 = lavas.get(i).y2;
			}
			if (grounds.get(i).x2 > blx2) {
				blx2 = grounds.get(i).x2;
			}
		}
		Ground bottom_limit = new Ground("bottom_limit", 0, bly1, blx2, bly1+50, "");
		grounds.add(bottom_limit);
		return bly1;
	}
	/**
	 * posa el limit superior del mapa
	 */
	private static void top_limit() {
		int tlx2 = 0;
		for (int i = 0; i < grounds.size(); i++) {
			if (grounds.get(i).x2 > tlx2) {
				tlx2 = grounds.get(i).x2;
			}
		}
		for (int i = 0; i < lavas.size(); i++) {
			if (lavas.get(i).x2 > tlx2) {
				tlx2 = lavas.get(i).x2;
			}
		}
		Ground top_limit = new Ground("top_limit", 0, -50, tlx2, 0, "");
		grounds.add(top_limit);
	}
	/**
	 * posa el limit dret del mapa
	 */
	private static int right_limit() {
		int rlx1 = 0;
		int rly2 = 0;
		for (int i = 0; i < grounds.size(); i++) {
			if (grounds.get(i).y2 > rly2) {
				rly2 = grounds.get(i).y2;
			}
			if (grounds.get(i).x2 > rlx1) {
				rlx1 = grounds.get(i).x2;
			}
		}
		for (int i = 0; i < lavas.size(); i++) {
			if (lavas.get(i).y2 > rly2) {
				rly2 = lavas.get(i).y2;
			}
			if (grounds.get(i).x2 > rlx1) {
				rlx1 = grounds.get(i).x2;
			}
		}
		Ground right_limit = new Ground("right_limit", rlx1, 0, rlx1+50, rly2, "");
		grounds.add(right_limit);
		return rlx1;
	}
	/**
	 * posa el limit esquerra del mapa
	 */
	private static void left_limit() {
		int lly2 = 0;
		for (int i = 0; i < grounds.size(); i++) {
			if (grounds.get(i).y2 > lly2) {
				lly2 = grounds.get(i).y2;
			}
		}
		for (int i = 0; i < lavas.size(); i++) {
			if (lavas.get(i).y2 > lly2) {
				lly2 = lavas.get(i).y2;
			}
		}
		Ground left_limit = new Ground("left_limit", -50, 0, 0, lly2, "");
		grounds.add(left_limit);
	}


}

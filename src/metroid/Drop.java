package metroid;

import motor.Sprite;

public abstract class Drop extends Sprite{

	public Drop(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		trigger = true;
		// TODO Auto-generated constructor stub
	}
	/**
	 * obliga a les clases filles a tenir el metode
	 */
	public abstract void action();
	/**
	 * fa que els drops del mapa segueixin a samus fins a colisionar amb ella
	 */
	public static void follow_samus() {
		int samus_x_mid = (((Main.samus.x2-Main.samus.x1)/2)-5)+Main.samus.x1;
		int samus_y_mid = (((Main.samus.y2-Main.samus.y1)/2)-5)+Main.samus.y1;
		for (int i = 0; i < Mapa.drops.size(); i++) {
			if (samus_x_mid-1 > Mapa.drops.get(i).x1 && samus_x_mid+1 > Mapa.drops.get(i).x1) {
				Mapa.drops.get(i).x1 += 2;
				Mapa.drops.get(i).x2 += 2;
			} else if (samus_x_mid-1 < Mapa.drops.get(i).x1 && samus_x_mid+1 < Mapa.drops.get(i).x1) {
				Mapa.drops.get(i).x1 -= 2;
				Mapa.drops.get(i).x2 -= 2;
			}
			
			if (samus_y_mid-1 > Mapa.drops.get(i).y1 && samus_y_mid+1 > Mapa.drops.get(i).y1) {
				Mapa.drops.get(i).y1 += 2;
				Mapa.drops.get(i).y2 += 2;
			} else if (samus_y_mid-1 < Mapa.drops.get(i).y1 && samus_y_mid+1 < Mapa.drops.get(i).y1) {
				Mapa.drops.get(i).y1 -= 2;
				Mapa.drops.get(i).y2 -= 2;
			}
		}
	}

}

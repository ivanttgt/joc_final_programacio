package metroid;

import java.util.ArrayList;
import java.io.*;
import java.util.Set;

import motor.Field;
import motor.Window;
import motor.Sprite;

public class Main {

	static public Field f = new Field();
	static public Window w = new Window(f);
	static public Samus samus = Samus.get();
	static public boolean viu = true;
	public static boolean end = false;
	private static int num_espais = 0;

	public static void main(String[] args) {
		samus.lastspawn.add(1);
		samus.lastspawn.add(1);
		samus.lastspawn.add(1);
		samus.lastspawn.add(1);
		samus.setlastspawn();
		HUD.get_huds();
		w.playMusic("sounds/song.wav");
		
		//insert song
		//w.playMusic("sounds/missile_shoot.wav");
		Mapa.nivel1();
		
		samus.add_ball_sprites();
		add_sprites(f);
		
		
		while (samus.lives > 0 && !end) {
			
			samus.passius();
			input();
			enemy.passius_enemies();
			
			Shoot.colisions();
			
			
			System.out.println(f.getScrollx());
			System.out.println(f.getScrolly());
			//System.out.println(Mapa.enemies.get(1).l_direction);
		//	System.out.println(samus);
			scroll_update();
			HUD.set_huds();

			f.draw();
			try {
				Thread.sleep(23);
				System.out.println(f.num);
				f.num = 0;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch blocka
				e.printStackTrace();
			}
			
		}
		w.close();
		System.out.println("Fi del joc");

	}
	/**
	 * comprova si samus ha de estar bloquejar al scroll o no, tambe el bloqueja a lo posicio adecuada
	 */
	private static void scroll_update() {
		if (Samus.get_actual_map() != 3) {
			boolean lock_x = false; 
			boolean lock_y = false; 
			//fer lock a X
			if (samus.x1 >= Mapa.h_start_scroll && samus.x2 <= Mapa.h_end_scroll) {
				lock_x = true;
				f.lockScroll(samus, w, lock_x, lock_y);
			} else {
				lock_x = false;
				f.lockScroll(samus, w, lock_x, lock_y);
				if(samus.x1 <= Mapa.h_start_scroll) {
					f.setScrollx(0);
				} else {
					f.setScrollx(-(Mapa.h_end_scroll-((w.getX()+40)/2)));
				}
			}
			
			//fer lock a Y
			if (samus.y1 >= Mapa.v_start_scroll && samus.y2 <= Mapa.v_end_scroll) {
				lock_y = true;
				f.lockScroll(samus, w, lock_x, lock_y);
			} else {
				lock_y = false;
				f.lockScroll(samus, w, lock_x, lock_y);
			if(samus.y1 <= Mapa.v_start_scroll) {
				f.setScrolly(0);
			} else {
				f.setScrolly(-(Mapa.v_end_scroll-((w.getY())/2)));
			}
			}
		}	
	}
	
	/**
	 * comprova el input
	 */
	private static void input() {
		
		Set<Character> keys_down = w.getKeysDown();
		
		// moviment del personatge, no usats en cas de stun
		if (samus.stun <= 0) {
			if (w.getPressedKeys().contains('d')) {
				samus.move_right();
			}
			if (w.getPressedKeys().contains('a')) {
				samus.move_left();
			}
			if (w.getPressedKeys().contains('w')) {
				samus.jump();
			}
			
			if (keys_down.contains('b')) {
				samus.bola();
			}
			if (keys_down.contains(' ')) {
				samus.shoot(f);
				num_espais++;
				String text = "Has presionat: " + num_espais + " vegades l'espai a la teva ultima partida.";
				try {
					PrintWriter pw = new PrintWriter("num_dispars_ultima_partida.txt");
					pw.print(text);
					pw.close();
				} catch (Exception e) {
					
				}
			}
			if (keys_down.contains('f')) {
				samus.swap_weapon();
				System.out.println(Samus.actual_weapon);
			}
			if (w.getPressedKeys().contains('n')) {
				samus.set_look_up_true();
			}
			if (!w.getPressedKeys().contains('n')) {
				samus.set_look_up_false();
			}
		}
		
		
		// altres que si que han de ser seguits
		if (!w.getPressedKeys().contains('d') && !w.getPressedKeys().contains('a') || samus.stun > 0) {
			samus.standing();
		}
	}
	
	/**
	 * carrega tots els sprites
	 * @param f
	 */
	public static void add_sprites(Field f){
		f.add(Mapa.grounds);
		f.add(Mapa.enemies);
		f.add(Mapa.doors);
		f.add(Mapa.destroyable_grounds);
		f.add(Mapa.Unlockers);
		f.add(samus.shoots);
		f.add(Main.samus);
		f.add(Mapa.lavas);
		f.add(Mapa.drops);
		f.add(samus.bombs);
		f.add(HUD.huds);
		f.add(HUD.sub_huds);
		
	
		
		//f.add(samus);
	}
}
package metroid;

import motor.Sprite;

public abstract class Unlocker extends Sprite{

	public Unlocker(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		trigger = true;
		// TODO Auto-generated constructor stub
	}
	/**
	 * obliga a tenir el metode a tots els seus decendens
	 */
	public abstract void action();

}

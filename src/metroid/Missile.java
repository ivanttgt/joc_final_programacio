package metroid;

public class Missile extends Shoot{
	
	public static int dmg = 30;

	public Missile(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path, dmg);
		this.x1 = x1;
		this.x2 = x2;
		this.y1 = y1;
		this.y2 = y2;
		this.path = path;
		trigger = true;
		// TODO Auto-generated constructor stub
	}
	/**
	 * crea i fa moure un misil a la dereta des de la posicio de samus
	 * @return
	 */
	public static Missile shoot_right() {
		Missile shoot = new Missile("", (Main.samus.x1 + 10), (Main.samus.y1 + 18), (Main.samus.x1 + 10) + 30, (Main.samus.y1 + 18) + 6, "sprites/Fx/shoot/misile_R_travel.gif");
		shoot.setVelocity(8, 0);
		return shoot;
	}
	/**
	 * crea i fa moure un misil a la esquerra des de la posicio de samus
	 * @return
	 */
	public static Missile shoot_left() {
		Missile shoot = new Missile("", (Main.samus.x1 + 10), (Main.samus.y1 + 18), (Main.samus.x1 + 10) + 30, (Main.samus.y1 + 18) + 6, "sprites/Fx/shoot/misile_L_travel.gif");
		shoot.setVelocity(-8, 0);
		return shoot;
	}
	/**
	 * crea i fa moure un misil cap amunt des de la posicio de samus
	 * @return
	 */
	public static Shoot shoot_up() {
		Missile shoot = null;
		if (Main.samus.l_direction) {
			shoot = new Missile("", (Main.samus.x1 + 26), (Main.samus.y1 + 0), (Main.samus.x1 + 26) + 6, (Main.samus.y1 + 0) + 30, "sprites/Fx/shoot/misile_T_travel.gif");
		} else {
			shoot = new Missile("", (Main.samus.x1 + 7), (Main.samus.y1 + 0), (Main.samus.x1 + 7) + 6, (Main.samus.y1 + 0) + 30, "sprites/Fx/shoot/misile_T_travel.gif");
		}
		shoot.setVelocity(0, -8);
		return shoot;
	}
}

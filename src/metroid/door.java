package metroid;

import motor.Sprite;

public class door extends Sprite {
	public String go_lvl;
	public int door_id;
	public int go_door_id;
	public boolean secret_door = false;
	
	public door(String name ,int id, int x1, int y1, int x2, int y2, String go_lvl, int go_door_id, String path, boolean secret) {
		super(name, x1, y1, x2, y2, path);
		this.go_lvl = go_lvl;
		this.door_id = id;
		this.go_door_id = go_door_id; 
		this.secret_door = secret;
		// TODO Auto-generated constructor stub
	}
	/**
	 * obre la porta per a que samus pugui pasar-hi
	 */
	public void open_door() {
		if (!secret_door) {
			if (this.path.contains("right")) {
				this.path = "sprites/map/doors/opened_right_door.png";
			} else {
				this.path = "sprites/map/doors/opened_left_door.png";
			}
		}
	}

}

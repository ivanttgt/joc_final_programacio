package metroid;

import motor.Sprite;
import motor.Field;

public class Shoot extends Sprite {
	
	public static int dmg;
	public Shoot(String name, int x1, int y1, int x2, int y2, String path, int dmg) {
		super(name, x1, y1, x2, y2, path);
		this.dmg = dmg;
		trigger = true;
		// TODO Auto-generated constructor stub
	}

	
	/**
	 * comprova les colisions dels dispars, enemics, restar vida i borrar enemic, i fa desapareixer el dispar si colisiona
	 */
	public static void colisions() {
		// destruir dispars
		// mirar si els dispars colisionen amb algo
		for (int i = 0; i < Samus.shoots.size(); i++) {
			// si xoca amb el mapa
			if (Samus.shoots.get(i).collidesWithList(Mapa.grounds).size() > 0) {
				System.out.println("paret");
				Samus.shoots.get(i).delete();
			}
			// si xoca amb algun enemic
			if (Samus.shoots.get(i).collidesWithList(Mapa.enemies).size() > 0) {
				
				// saber amb quin enemic esta colisionant
				for (int j = 0; j < Mapa.enemies.size(); j++) {
					if (Mapa.enemies.get(j).collidesWith(Samus.shoots.get(i))) {
						
						if (Mapa.enemies.get(j) instanceof Boss) {
							if (Samus.shoots.get(i) instanceof Missile) {
								Mapa.enemies.get(j).hp -= dmg;
								if (Mapa.enemies.get(j).hp <= 0) {
									Main.end = true;
								}
							}
						} else {
							Mapa.enemies.get(j).hp -= dmg;
						}
						System.out.println(Mapa.enemies.get(j).hp);
						Samus.shoots.get(i).delete();
						// mirar vida enemic i fer-lo morir si esta a 0
						if (Mapa.enemies.get(j).hp <= 0) {
							System.out.println("mort");
							Drops.enemy_drop(Mapa.enemies.get(j));
							Mapa.enemies.get(j).delete();
							Mapa.enemies.remove(Mapa.enemies.get(j));
							Main.add_sprites(Main.f);
						}
						
					}
				}
			}
			// si colisiona amb alguna porta i amb quina
			if (Samus.shoots.get(i).collidesWithList(Mapa.doors).size() > 0) {
				// saber amb quin enemic esta colisionant
				for (int j = 0; j < Mapa.doors.size(); j++) {
					if (Mapa.doors.get(j).collidesWith(Samus.shoots.get(i)) && Mapa.doors.get(j).path.contains("closed") && Mapa.doors.get(j) instanceof shielded_door && Samus.shoots.get(i) instanceof Missile) {
						Mapa.doors.get(j).open_door();
						Samus.shoots.get(i).delete();
					}
					else if (Mapa.doors.get(j).collidesWith(Samus.shoots.get(i)) && Mapa.doors.get(j).path.contains("closed") && Mapa.doors.get(j) instanceof shielded_door && !(Samus.shoots.get(i) instanceof Missile)) {
						Main.w.playSFX("sounds/error_door.wav");
						Samus.shoots.get(i).delete();
					}
					else if (Mapa.doors.get(j).collidesWith(Samus.shoots.get(i)) && Mapa.doors.get(j).path.contains("closed") && !(Mapa.doors.get(j) instanceof shielded_door)) {
						Mapa.doors.get(j).open_door();
						Samus.shoots.get(i).delete();
					} else if (Mapa.doors.get(j).collidesWith(Samus.shoots.get(i))) {
						Samus.shoots.get(i).delete();
					}
				}
			}
			if (Samus.shoots.get(i).collidesWithList(Mapa.destroyable_grounds).size()>0) {
				Samus.shoots.get(i).delete();
			}
		}
	}

}

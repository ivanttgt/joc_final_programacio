package metroid;

public class Ball_Unlocker extends Unlocker{
	
	private static boolean created = false;

	public Ball_Unlocker(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		// TODO Auto-generated constructor stub
	}
	/**
	 * desbloqueija la habilitat de fer-se bola i tirar bombes de samus
	 */
	@Override
	public void action() {
		Main.samus.ball_unlocked = true;
	}
	/**
	 * retorna el desbloquejador, en cas de no existir, el crea
	 * @return
	 */
	public static Unlocker get_unlocker() {
		Ball_Unlocker m_unlocker = null;
		if (created) {
			m_unlocker.x1 = Mapa.misile_unlocker.x1;
			m_unlocker.x2 = Mapa.misile_unlocker.x2;
			m_unlocker.y1 = Mapa.misile_unlocker.y1;
			m_unlocker.y2 = Mapa.misile_unlocker.y2;
			m_unlocker.name = Mapa.misile_unlocker.name;
			m_unlocker.path = Mapa.misile_unlocker.path;
		} else {
			// nivell 1
			m_unlocker = new Ball_Unlocker("missile_unlocker", 400, 350, 440, 390, "sprites/drops/unlock_bomb.gif");

			// nivell2
			 //samus = new Samus("Samus", 416, 339, 456, 389, 99, 10, "sprites/Samus/Rs_standing.PNG");
			 
			 //max_hp=99, x1=416, y1=339, x2=456, y2=389, hp=9, num_misils=10
			created = true;
		}
		return m_unlocker;
	}

}

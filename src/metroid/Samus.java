package metroid;

import motor.Sprite;

import java.awt.Window;
import java.util.ArrayList;

import motor.Field;

public class Samus extends Personatge {

	boolean al_terra = true;
	static ArrayList<Shoot> shoots = new ArrayList<Shoot>();
	static ArrayList<Bomb> bombs = new ArrayList<Bomb>();
	static ArrayList<String> samus_ball_sprites = new ArrayList<String>();
	public static boolean samus_ball = false;
	public static boolean missile_unlocked = false;
	public static boolean ball_unlocked = false;
	public int num_samus_ball_sprite = 0;
	public static int actual_weapon = 1;
	ArrayList<Integer> lastspawn = new ArrayList<Integer>();
	public static int lives = 3;
	public static int stun = 0;
	private boolean look_up = false;
	private static int actual_map = 0;
	private static int shoot_delay = 0;
	private static int missile_delay = 0;
	private static int bomb_delay = 0;

	/**
	 * si es true es a la dereta
	 */
	static boolean l_direction = true;
	public int hp;
	public int max_hp;
	public int num_missiles;
	public int max_num_missiles;
	private static boolean created = false;

	private Samus(String name, int x1, int y1, int x2, int y2, int max_hp, int max_num_missiles, String path) {
		super("samus", x1, y1, x2, y2, 99, path);
		// TODO Auto-generated constructor stub
		this.max_num_missiles = max_num_missiles;
		this.num_missiles = 0;
		this.max_hp = max_hp;
		this.hp = max_hp;
		physicBody = true;
		solid = true;
		Samus.this.setConstantForce(0, 1.2);

	}
	
	
	public static int get_actual_map() {
		return actual_map;
	}
	
	public static void set_actual_map(int num) {
		actual_map = num;
	}
	
	/**
	 * indica que samus miri amunt
	 */
	public void set_look_up_true() {
		look_up = true;
	}
	/**
	 * indica que samus deixa de mirar amunt
	 */
	public void set_look_up_false() {
		look_up = false;
	}

	/**
	 * Retorna les variables de samus, en cas ne no existir, en crea un.
	 * 
	 * @return
	 */
	public static Samus get() {
		Samus samus = null;
		if (created) {
			samus.x1 = Main.samus.x1;
			samus.x2 = Main.samus.x2;
			samus.y1 = Main.samus.y1;
			samus.y2 = Main.samus.y2;
			samus.name = Main.samus.name;
			samus.path = Main.samus.path;
			samus.hp = Main.samus.hp;
			samus.max_hp = Main.samus.max_hp;
			samus.num_missiles = 0;
			samus.max_num_missiles = Main.samus.max_num_missiles;
		} else {
			// nivell 1
			samus = new Samus("Samus", 30, 100, 70, 150, 99, 15, "sprites/Samus/Rs_standing.PNG");

			// nivell3
			// samus = new Samus("Samus", 10, 10, 40, 50, 99, 10, "sprites/Samus/Rs_standing.PNG");
			 
			 //max_hp=99, x1=416, y1=339, x2=456, y2=389, hp=9, num_misils=10
			created = true;
		}
		return samus;
	}
	/**
	 * permet imprimir samus per consola
	 */
	@Override
	public String toString() {
		return "Samus [max_hp=" + max_hp + ", x1=" + x1 + ", y1=" + y1 + ", x2=" + x2 + ", y2=" + y2 + ", hp=" + hp
				+ ", num_misils=" + num_missiles + "]";
	}

	/**
	 * afageix tots els sprites de la transformacio en bola a una llista d'aquests
	 */
	public void add_ball_sprites() {
		samus_ball_sprites.add("sprites/Samus/ball-0.png");
		samus_ball_sprites.add("sprites/Samus/ball-1.png");
		samus_ball_sprites.add("sprites/Samus/ball-2.png");
		samus_ball_sprites.add("sprites/Samus/ball-3.png");
		samus_ball_sprites.add("sprites/Samus/ball-4.png");
		samus_ball_sprites.add("sprites/Samus/ball-5.png");
		samus_ball_sprites.add("sprites/Samus/ball-6.png");
		samus_ball_sprites.add("sprites/Samus/ball-7.png");
		samus_ball_sprites.add("sprites/Samus/ball-8.png");
		samus_ball_sprites.add("sprites/Samus/ball-9.png");
		samus_ball_sprites.add("sprites/Samus/ball-10.png");
		samus_ball_sprites.add("sprites/Samus/ball-11.png");
		samus_ball_sprites.add("sprites/Samus/ball-12.png");
		samus_ball_sprites.add("sprites/Samus/ball-13.png");
		samus_ball_sprites.add("sprites/Samus/ball-14.png");
		samus_ball_sprites.add("sprites/Samus/ball-15.png");
		samus_ball_sprites.add("sprites/Samus/ball-16.png");
		samus_ball_sprites.add("sprites/Samus/ball-17.png");
		samus_ball_sprites.add("sprites/Samus/ball-18.png");
		samus_ball_sprites.add("sprites/Samus/ball-19.png");
		samus_ball_sprites.add("sprites/Samus/ball-20.png");
		samus_ball_sprites.add("sprites/Samus/ball-21.png");
		samus_ball_sprites.add("sprites/Samus/ball-22.png");
		samus_ball_sprites.add("sprites/Samus/ball-23.png");
		samus_ball_sprites.add("sprites/Samus/ball-24.png");
		samus_ball_sprites.add("sprites/Samus/ball-25.png");

	}

	/**
	 * comprova gravetat, si samus pot saltar, si mor, els cooldowns, les colisions
	 * de samus... tot el que est� relacionat amb comprobacions continues
	 * 
	 * @param f
	 */
	public void passius() {
		cooldown_bomba();
		delays();
		capacitat_salt();
		colisions_hostils();
		colisions_aliades();
		pasar_portes();
		Drop.follow_samus();
	}
	private void delays() {
		shoot_delay --;
		missile_delay --;
		bomb_delay --;
	}


	/**
	 * comprova les colisions beneficioses per a samus
	 */
	private void colisions_aliades() {
		for (int i = 0; i < Mapa.drops.size(); i++) {
			if (Mapa.drops.get(i).collidesWith(this)) {
				Mapa.drops.get(i).action();
				Main.w.playSFX("sounds/drop_picked.wav");
				Mapa.drops.get(i).delete();
			}
		}
		
		for (int i = 0; i < Mapa.Unlockers.size(); i++) {
			if (Mapa.Unlockers.get(i).collidesWith(this)) {
				Mapa.Unlockers.get(i).action();
				Mapa.Unlockers.get(i).delete();
			}
		}
		
	}

	/**
	 * Aplica cooldown a la bomba i determina cuan explota
	 */
	private void cooldown_bomba() {
		for (int i = 0; i < bombs.size();) {
			i = bombs.get(i).explotar(i);
		}
	}

	/**
	 * comprova i determina si samus pot saltar o no
	 */
	private void capacitat_salt() {
		// comprovar cuan pot saltar
		if (this.velocity[1] == 0 && this.isGrounded(Main.f)) {
			al_terra = true;
			this.setForce(0, 0);
		}
		if (this.velocity[1] != 0) {
			al_terra = false;
		}
	}

	/**
	 * Comprova totes les colisions que poden danyar a samus i fa que mori en cas de
	 * arribar a 0 hp
	 */
	private void colisions_hostils() {
		stun--;
		// colisions amb laves
		if (this.collidesWithList(Mapa.lavas).size() > 0) {
			Main.samus.hp--;
		}

		// si colisiona amb algun enemic
		if (this.collidesWithList(Mapa.enemies).size() > 0) {
			// per evitar si colisiones per avaix morir en 4 frames
			if (stun <= 0) {
				stun = 10;
				this.hp -= 30;
				if (this.hp > 0) {
					if (l_direction) {
						Samus.this.setForce(-4.5, -4.5);
					} else {
						Samus.this.setForce(4.5, -4.5);
					}
				}
			}
		}

		if (Main.samus.hp <= 0) {
			if (!samus_ball) {
				this.x1 = lastspawn.get(0);
				this.y1 = lastspawn.get(1);
				this.x2 = lastspawn.get(2);
				this.y2 = lastspawn.get(3);
			} else {
				// test2
				this.x1 = lastspawn.get(0) + 10;
				this.y1 = lastspawn.get(1) + 30;
				this.x2 = lastspawn.get(2) - 10;
				this.y2 = lastspawn.get(3);
			}
			Main.samus.hp = Main.samus.max_hp;
			lives--;
			stun = 0;
		}
	}

	/**
	 * Comprova si samus colisiona amb algunaporta, en cas de fer-ho, i d'estar
	 * aquesta oberta pasa a traves de aquesta
	 */
	private void pasar_portes() {
		// miar si colisiona amb alguna porta en cas de que estigui oberta
		if (this.collidesWithList(Mapa.doors).size() > 0) {
			// saber amb quina porta colisiona
			for (int j = 0; j < Mapa.doors.size(); j++) {
				if (Mapa.doors.get(j).collidesWith(this)) {
					if (Mapa.doors.get(j).path.contains("opened")) {
						String map_to_load = Mapa.doors.get(j).go_lvl;
						int door_id_to_go = Mapa.doors.get(j).go_door_id;
						Main.f.clear();
						switch (map_to_load) {
						case "nivell1":
							Mapa.nivel1();
							break;
						case "nivell2":
							Mapa.nivel2();
							break;
						case "nivell3":
							 Mapa.nivel3();
							break;
						default:
							break;
						}
						// posar samus a la porta corresponent
						for (int i = 0; i < Mapa.doors.size(); i++) {

							if (Mapa.doors.get(i).door_id == door_id_to_go) {
								if (Mapa.doors.get(i).path.contains("right")) {
									if (!samus_ball) {
										this.x1 = Mapa.doors.get(i).x1 - 45;
										this.y1 = Mapa.doors.get(i).y1 + 19;
										this.x2 = this.x1 + 40;
										this.y2 = this.y1 + 50;
									} else {
										this.x1 = Mapa.doors.get(i).x1 - 45;
										this.y1 = Mapa.doors.get(i).y1 + 19;
										this.x2 = this.x1 + 40;
										this.y2 = this.y1 + 50;
										this.x1 += 20;
										this.y1 += 30;
									}
								} else {
									if (!samus_ball) {
										this.x1 = Mapa.doors.get(i).x1 + 45;
										this.y1 = Mapa.doors.get(i).y1 + 19;
										this.x2 = this.x1 + 40;
										this.y2 = this.y1 + 50;
									} else {
										this.x1 = Mapa.doors.get(i).x1 + 45;
										this.y1 = Mapa.doors.get(i).y1 + 19;
										this.x2 = this.x1 + 40;
										this.y2 = this.y1 + 50;
										this.x1 += 20;
										this.y1 += 30;
									}
								}

							}
						}

						Main.add_sprites(Main.f);
						setlastspawn();

					}
				}
			}
		}
	}

	/**
	 * mou samus cap a la dereta
	 */
	public void move_right() {
		Samus.this.setVelocity(5, velocity[1]);
		if (!samus_ball) {
			path = "sprites/Samus/Rs_run.gif";
		} else {
			if (num_samus_ball_sprite <= samus_ball_sprites.size() - 1 && num_samus_ball_sprite >= 0) {
				path = samus_ball_sprites.get(num_samus_ball_sprite);
				num_samus_ball_sprite++;
			} else {
				num_samus_ball_sprite = 0;
				path = samus_ball_sprites.get(num_samus_ball_sprite);
			}
		}

		l_direction = true;
		// Main.samus.getCeiling(f);
	}

	/**
	 * mou samus cap a la esquerra
	 */
	public void move_left() {
		Samus.this.setVelocity(-5, velocity[1]);
		if (!samus_ball) {
			path = "sprites/Samus/Ls_run.gif";
		} else {
			if (num_samus_ball_sprite <= samus_ball_sprites.size() - 1 && num_samus_ball_sprite >= 0) {
				path = samus_ball_sprites.get(num_samus_ball_sprite);
				num_samus_ball_sprite--;
			} else {
				num_samus_ball_sprite = samus_ball_sprites.size() - 1;
				path = samus_ball_sprites.get(num_samus_ball_sprite);
			}
		}
		l_direction = false;
	}

	/**
	 * fa que samus salti en cas de que estigui tocant el terra
	 */
	public void jump() {
		if (al_terra) {
			Main.w.playSFX("sounds/jump.wav");
			if (!samus_ball) {
				Samus.this.setForce(0, -7);
			} else {
				Samus.this.setForce(0, -6.5);
			}

			al_terra = false;
		}
	}

	/**
	 * posa sprite corresponent a la ultima direccio de samus
	 */
	public void standing() {
		if (l_direction) {
			if (look_up && !samus_ball) {
				path = "sprites/Samus/look_up_right_standing.PNG";
			} else if (!samus_ball) {
				path = "sprites/Samus/Rs_standing.PNG";
			}

		} else {
			if (look_up && !samus_ball) {
				path = "sprites/Samus/look_up_left_standing.PNG";
			} else if (!samus_ball) {
				path = "sprites/Samus/Ls_standing.PNG";
			}
		}
		Samus.this.setVelocity(0, velocity[1]);
	}

	/**
	 * comprova la direcci� on mira samus i crea un dispar donant-li velocitat
	 * adecuada
	 * 
	 * si samus esta en mode bola tira una bomba que explota passat un rato
	 * 
	 * @param f
	 */
	public void shoot(Field f) {
		Shoot shoot = null;
		Bomb bomb = null;
		if (!samus_ball) {
			if (actual_weapon == 1 && shoot_delay < 1) {
				shoot_delay = 5;
				Main.w.playSFX("sounds/standard_shoot_sound.wav");
				if (look_up) {
					shoot = Standard_shoot.shoot_up();
				} else if (l_direction) {
					shoot = Standard_shoot.shoot_right();

				} else {
					shoot = Standard_shoot.shoot_left();
				}
				shoots.add(shoot);
			} else if (missile_delay < 1 && actual_weapon != 1 ) {
				missile_delay = 15;
				if (num_missiles > 0) {
					Main.w.playSFX("sounds/missile_shoot.wav");
					if (look_up) {
						shoot = Missile.shoot_up();
					} else if (l_direction) {
						shoot = Missile.shoot_right();
					} else {
						shoot = Missile.shoot_left();
					}
					shoots.add(shoot);
					num_missiles--;
				}

			}
			Main.add_sprites(f);
		}
		// tirar bombes
		else if (bomb_delay < 1) {
			bomb_delay = 10;
			if (bombs.size() < 4) {
				bomb = new Bomb("Shoot", x1 + 5, y1 + 10, x2 - 5, y2, "sprites/ball_bomb/bomb.gif");
				bombs.add(bomb);
				Main.add_sprites(f);
			}
		}
		if (actual_map == 3) {
			Main.f.add(Boss.camp);
		}
	}

	/**
	 * Transforma samus en bola o en persona amb un toogle a un bole�
	 */
	public void bola() {
		if (ball_unlocked) {
			if (!samus_ball) {
				samus_ball = true;
				path = samus_ball_sprites.get(0);
				x1 += 10;
				x2 -= 10;
				y1 += 30;
			} else {
				samus_ball = false;
				x1 -= 10;
				x2 += 10;
				y1 -= 30;
				angle = 0;
				this.physicBody = false;
				if (this.collidesWithList(Mapa.grounds).size() > 0
						|| this.collidesWithList(Mapa.destroyable_grounds).size() > 0
						|| this.collidesWithList(Mapa.doors).size() > 0) {
					x1 += 10;
					x2 -= 10;
					y1 += 30;
					samus_ball = true;
				}
				this.physicBody = true;
			}
		}
		
	}
	/**
	 * canvia de arma intercalant entre misils i dispars stendard
	 */
	public void swap_weapon() {
		if (missile_unlocked && !samus_ball) {
			actual_weapon++;
			if (actual_weapon > 2 ) {
				actual_weapon = 1;
			}
		}
		
	}
	/**
	 * indica el spawn de samus al morir
	 */
	public void setlastspawn() {
		if (!samus_ball) {
			lastspawn.set(0, this.x1);
			lastspawn.set(1, this.y1);
			lastspawn.set(2, this.x2);
			lastspawn.set(3, this.y2);
		} else {
			lastspawn.set(0, this.x1 - 10);
			lastspawn.set(1, this.y1 - 30);
			lastspawn.set(2, this.x2 + 10);
			lastspawn.set(3, this.y2);
		}

	}

}
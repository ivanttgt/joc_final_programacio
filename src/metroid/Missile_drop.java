package metroid;

public class Missile_drop extends Drop{

	public Missile_drop(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		// TODO Auto-generated constructor stub
	}
	/**
	 * suma 3 als misils actuals de samus fins que aquests estan plens
	 */
	@Override
	public void action() {
		Main.samus.num_missiles += 5;
		if (Main.samus.num_missiles > Main.samus.max_num_missiles) {
			Main.samus.num_missiles = Main.samus.max_num_missiles;
		}
	}

}

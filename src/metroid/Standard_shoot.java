package metroid;

public class Standard_shoot extends Shoot{
	
	public static int dmg = 10;

	public Standard_shoot(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path, dmg);
		this.x1 = x1;
		this.x2 = x2;
		this.y1 = y1;
		this.y2 = y2;
		this.path = path;
		trigger = true;
	}
	/**
	 * crea i mou a la dereta un dispar des de la ubicacio de samus
	 * @return
	 */
	public static Standard_shoot shoot_right() {
		Standard_shoot shoot = new Standard_shoot("", (Main.samus.x1 + 10), (Main.samus.y1 + 18), (Main.samus.x1 + 10) + 30, (Main.samus.y1 + 18) + 3, "sprites/Fx/shoot/standard_shoot.png");
		shoot.setVelocity(15, 0);
		return shoot;
	}
	/**
	 * crea i mou a la esquerra un dispar des de la ubicacio de samus
	 * @return
	 */
	public static Standard_shoot shoot_left() {
		Standard_shoot shoot = new Standard_shoot("", (Main.samus.x1 + 10), (Main.samus.y1 + 18), (Main.samus.x1 + 10) + 30, (Main.samus.y1 + 18) + 3, "sprites/Fx/shoot/standard_shoot.png");
		shoot.setVelocity(-15, 0);
		return shoot;
	}
	/**
	 * crea i mou cap amunt un dispar des de la ubicacio de samus
	 * @return
	 */
	public static Shoot shoot_up() {
		Standard_shoot shoot = null;
		if (Main.samus.l_direction) {
			shoot = new Standard_shoot("", (Main.samus.x1 + 28), (Main.samus.y1 + 3), (Main.samus.x1 + 28) + 3, (Main.samus.y1 + 3) + 30, "sprites/Fx/shoot/standard_shoot.png");
		} else {
			shoot = new Standard_shoot("", (Main.samus.x1 + 8), (Main.samus.y1 + 3), (Main.samus.x1 + 8) + 3, (Main.samus.y1 + 3) + 30, "sprites/Fx/shoot/standard_shoot.png");	
		}
		shoot.setVelocity(0, -15);
		return shoot;		
	}
}

package metroid;

public class foc extends enemy{

	private int hp = 1;
	
	int foc_cooldown;
	boolean collided = false;
	public foc(String name, int x1, int y1, int x2, int y2) {
		super(name, x1, y1, x2, y2, "sprites/enemies/boss/foc.gif");
		this.setVelocity(0, -2);
		trigger = true;
		this.foc_cooldown = 100;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void passius() {
		colisions();
	}

	private void colisions() {
		if (this.collidesWith(Main.samus)) {
			Mapa.enemies.remove(this);
			Main.add_sprites(Main.f);
			Main.f.add(Boss.camp);
		} else if (this.collidesWithList(Mapa.grounds).size()>0) {
			trigger = false;
			solid = true;
			if (!this.collided) {
				this.collided = true;
				this.x1 -= 5;
				this.x2 += 5;
			}
			this.setVelocity(0, 0);
			this.foc_cooldown--;
			if (this.foc_cooldown == 0) {
				Mapa.enemies.remove(this);
				Main.add_sprites(Main.f);
				Main.f.add(Boss.camp);
			}
		}
	}

}
